require('dotenv').config()
const express = require('express');
var cors = require('cors');//cors
const { param } = require('express/lib/request');
const app = express();
const convertRoutes = require('./routes/convertRoutes');

app.use(cors());//cors
app.use(
    express.urlencoded({
        extended: true
    })
)

app.use(express.json())

app.use('/convert', convertRoutes)

app.get('/test', (req,res)=>{
    fetch('https://economia.awesomeapi.com.br/json/last/USD-BRL,EUR-BRL').then(res=>res.text()).then(text=>console.log(text)).then(res.json({message: "Oi"}))
})


app.listen(5000)

